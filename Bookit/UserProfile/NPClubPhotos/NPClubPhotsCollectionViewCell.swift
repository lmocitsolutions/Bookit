//
//  NPClubPhotsCollectionViewCell.swift
//  Bookit
//
//  Created by Ranjan on 23/12/21.
//

import UIKit

class NPClubPhotsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viw:UIView!{
        didSet{
            viw.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var img:UIImageView!{
        didSet{
            
            img.layer.cornerRadius = 10.0
            img.clipsToBounds = true
        }
    }
    
}
