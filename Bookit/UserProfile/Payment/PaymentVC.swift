//
//  PaymentVC.swift
//  Bookit
//
//  Created by Ranjan on 07/01/22.
//

import UIKit
import Alamofire

class PaymentVC: UIViewController,UITextFieldDelegate {
    
    var am_from_which_profile:String!
    
    var get_club_name:String!
    
    var str_advance:String! = "50"
    
    var total_price:String! = "0"
    
    var is_full_payment_status:String! = "2"
    
    var dict_get_table_Details:NSDictionary!
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "PAYMENT CONFIRM"
                lblNavigationTitle.textColor = .white
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var cardNumberCursorPreviousPosition = 0
    
    var strCardType:String!
    
    @IBOutlet weak var tbleView:UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
        }
    }
    
    @IBOutlet weak var viewTop:UIView! {
        didSet {
            viewTop.backgroundColor = BUTTON_DARK_APP_COLOR
            
        }
    }
    
    @IBOutlet weak var lblClubTableInfo:UILabel!
    
    @IBOutlet weak var btnmakePayment:UIButton! {
        didSet {
            btnmakePayment.backgroundColor = NAVIGATION_COLOR
            btnmakePayment.setTitle("MAKE PAYMENT", for: .normal)
            btnmakePayment.setTitleColor(.white, for: .normal)
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
         // print(self.dict_get_table_Details as Any)
        
        /*
         clubTableId = 2;
         created = "Dec 21st, 2021, 3:51 pm";
         image = "";
         name = Xyz;
         "profile_picture" = "";
         seatPrice = 75;
         totalSeat = 5;
         userAddress = gggggggg;
         userId = 2;
         userName = "Raushan Kumar";
         */
        
        self.btnBack.addTarget(self, action: #selector(back_click_method), for: .touchUpInside)
        
        if self.am_from_which_profile == "booking_Details" {
            
            print(self.dict_get_table_Details as Any)
            
            /*
             Clubbanner = "";
             ClubcontactNumber = 7428171872;
             Clubemail = "raushan@mailinator.com";
             ClubfullName = "Raushan Kumar";
             Clubimage = "https://demo4.evirtualservices.net/bookit/img/uploads/users/16400815972.jpg";
             ClublastName = "";
             Tableimage = "https://demo4.evirtualservices.net/bookit/img/uploads/table/16400820692.jpg";
             TableseatPrice = 75;
             TabletotalSeat = 5;
             Userimage = "";
             advancePayment = "187.5";
             bokingDate = "";
             bookingId = 15;
             clubId = 2;
             clubTableId = 2;
             contactNumber = 1232142314;
             created = "2022-01-19 12:53:00";
             email = "ios@gmail.com";
             fullName = ios;
             fullPaymentStatus = 2;
             lastName = "";
             seatPrice = 75;
             tableName = Xyz;
             totalAmount = 375;
             totalSeat = 5;
             userId = 6;
             
             */
            
            self.lblClubTableInfo.text = (self.dict_get_table_Details["ClubfullName"] as! String)+" - Table '\(self.dict_get_table_Details["tableName"] as! String)' Booking"
            
            // let indexPath = IndexPath.init(row: 0, section: 0)
            // let cell = self.tbleView.cellForRow(at: indexPath) as! PaymentTableViewCell
            
            
            
        } else {
        
            self.lblClubTableInfo.text = String(self.get_club_name)+" - Table '\(self.dict_get_table_Details["name"] as! String)' Booking"
            
            self.view.backgroundColor = .white
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            
            self.strCardType = "none"
        
            self.btnmakePayment.addTarget(self, action: #selector(checkValidation), for: .touchUpInside)
            
            
            
        }
        
        
    }
    
    @objc func back_click_method() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func checkValidation(){
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PaymentTableViewCell
        if cell.txtCardNumber.text == "" {
            let alert = UIAlertController(title: "Card Number", message: "card number should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        /*else if cell.txtNameOnCard.text == "" {
            let alert = UIAlertController(title: "Name", message: "Name should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }*/
        
        else if cell.txtExpDate.text == "" {
            let alert = UIAlertController(title: "Exp Month", message: "Expiry Month should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        else if cell.txtCVV.text == "" {
            let alert = UIAlertController(title: "Security Code", message: "Security Code should not be blank", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        else {
            
            // let indexPath = IndexPath.init(row: 0, section: 0)
            // let cell = self.tbleView.cellForRow(at: indexPath) as! PaymentTableViewCell
            //lblEXPDate.isHidden = false
            //lblEXPDate.text = cell.txtExpDate.text
            //lblCardHolderName.isHidden=false
            //lblCardHolderName.text = cell.txtNameOnCard.text
            //lblCardNumberHeading.isHidden = false
            //lblCardNumberHeading.text = cell.txtCardNumber.text
           // imgCardImage.image = UIImage(named: String(strCardType))
            
            self.book_a_table_wb()
        }
    }

    
    
    @objc func book_a_table_wb() {
        
        self.view.endEditing(true)
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
            
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
            
            
            let x_2 : Int = (self.dict_get_table_Details["userId"] as! Int)
            let myString_2 = String(x_2)
            
            let x_3 : Int = (self.dict_get_table_Details["clubTableId"] as! Int)
            let myString_3 = String(x_3)
            
            let x_4 : Int = (self.dict_get_table_Details["totalSeat"] as! Int)
            let myString_4 = String(x_4)
            
            let x_5 : Int = (self.dict_get_table_Details["seatPrice"] as! Int)
            let myString_5 = String(x_5)
            
            let total_amount_minus_admin = Double(myString_5)! - Double(0)
            
            // print(self.total_price as Any)
            
            var str_advance_payment_is:String!
            
            if self.is_full_payment_status == "2" {
                
                str_advance_payment_is = String(self.total_price)
            } else {
                
                str_advance_payment_is = "0"
            }
            
            
            
            let total_amount_calculate = Double(myString_4)!*Double(myString_5)!
            
            
            
            
            let params = customer_book_a_table(action   : "addbooking",
                                           userId       : myString,
                                           clubId       : myString_2,
                                           clubTableId  : myString_3,
                                           bookingDate  : Date.getCurrentDate(),
                                           arrvingTime  : "",
                                           totalSeat    : myString_4,
                                           seatPrice    : myString_5,
                                           adminFee     : "0",
                                           totalAmount  : String(total_amount_calculate),
                                           advancePayment : String(str_advance_payment_is),
                                           fullPaymentStatus: String(self.is_full_payment_status))
        
        print(params as Any)
        
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
            // debugPrint(response.result)
            
            switch response.result {
            case let .success(value):
                
                let JSON = value as! NSDictionary
                print(JSON as Any)
                
                var strSuccess : String!
                strSuccess = (JSON["status"]as Any as? String)?.lowercased()
                print(strSuccess as Any)
                if strSuccess == String("success") {
                    print("yes")
                    
                    ERProgressHud.sharedInstance.hide()
                    
                    self.confirm_payment()
                    
                } else {
                    print("no")
                    //  ERProgressHud.sharedInstance.hide()
                    
                    var strSuccess2 : String!
                    strSuccess2 = JSON["msg"]as Any as? String
                    
                    if strSuccess2 == "Your Account is Inactive. Please contact admin.!!" ||
                        strSuccess2 == "Your Account is Inactive. Please contact admin.!" ||
                        strSuccess2 == "Your Account is Inactive. Please contact admin." {
                        
                        
                    } else {
                        
                        let alert = UIAlertController(title: String(strSuccess).uppercased(), message: String(strSuccess2), preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        
                        self.present(alert, animated: true)
                        
                    }
                }
                
            case let .failure(error):
                print(error)
                ERProgressHud.sharedInstance.hide()
                
                // Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
            }
        }
         }
    }

}


extension PaymentVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PaymentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PaymentTableCell") as! PaymentTableViewCell
        
        cell.backgroundColor = .white
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.txtCardNumber.delegate = self
        cell.txtExpDate.delegate = self
        cell.txtCVV.delegate = self
        //cell.txtNameOnCard.delegate = self
        
        if self.am_from_which_profile == "booking_Details" {
            
            cell.btnAdvance.backgroundColor = .lightGray
            cell.btnAdvance.layer.cornerRadius = 8.0
            cell.btnAdvance.clipsToBounds = true
            cell.btnAdvance.tintColor = .white
            cell.btnAdvance.isUserInteractionEnabled = false
            
            cell.btnTotal.backgroundColor = NAVIGATION_COLOR
            cell.btnTotal.layer.cornerRadius = 8.0
            cell.btnTotal.clipsToBounds = true
            cell.btnTotal.tintColor = .black
            cell.btnTotal.isUserInteractionEnabled = false
            
            // total amount
            let total_amount_get:String!
            
            if self.dict_get_table_Details["totalAmount"] is String {
                print("Yes, it's a String")

                total_amount_get = (self.dict_get_table_Details["totalAmount"] as! String)
                
            } else if self.dict_get_table_Details["totalAmount"] is Int {
                print("It is Integer")
                            
                let x2 : Int = (self.dict_get_table_Details["totalAmount"] as! Int)
                let myString2 = String(x2)
                
                total_amount_get = String(myString2)
                
            } else {
                print("i am number")
                            
                let temp:NSNumber = self.dict_get_table_Details["totalAmount"] as! NSNumber
                let tempString = temp.stringValue
                
                total_amount_get = String(tempString)
            }
            
            
            // advance payment
            let advance_payment_get:String!
            
            if self.dict_get_table_Details["advancePayment"] is String {
                print("Yes, it's a String")

                advance_payment_get = (self.dict_get_table_Details["advancePayment"] as! String)
                
            } else if self.dict_get_table_Details["advancePayment"] is Int {
                print("It is Integer")
                            
                let x2 : Int = (self.dict_get_table_Details["advancePayment"] as! Int)
                let myString2 = String(x2)
                
                advance_payment_get = String(myString2)
                
            } else {
                print("i am number")
                            
                let temp:NSNumber = self.dict_get_table_Details["advancePayment"] as! NSNumber
                let tempString = temp.stringValue
                
                advance_payment_get = String(tempString)
            }
            
            let get_pending_amount = Double(total_amount_get)!-Double(advance_payment_get)!
            
            cell.lbl_total_amount.text = "Total seat price is : $"+String(total_amount_get)
            
            cell.btnTotal.setTitle("Pay : $\(get_pending_amount)", for: .normal)
            cell.btnTotal.setTitleColor(.white, for: .normal)
            
            
            cell.btnAdvance.setTitle("Already Pay : $"+String(advance_payment_get), for: .normal)
            cell.btnTotal.setTitle("Left : $"+String(get_pending_amount), for: .normal)
            
            self.btnmakePayment.setTitle("Make Payment $\(get_pending_amount) ", for: .normal)
            self.total_price = String(get_pending_amount)
            
        } else {
        
            let x : Int = self.dict_get_table_Details["seatPrice"] as! Int
            let myString = String(x)
            
            let x_2 : Int = self.dict_get_table_Details["totalSeat"] as! Int
            let myString_2 = String(x_2)
            
            let total_amount_calculate = Double(myString)!*Double(myString_2)!
            // print(total_amount_calculate as Any)
            
            cell.lbl_total_amount.text = "Total seat price is : $"+String(total_amount_calculate)
            cell.lbl_total_amount.textColor = .black
            
            // show full price
            cell.btnTotal.setTitle("Pay Full : $\(total_amount_calculate)", for: .normal)
            
            cell.btnAdvance.setTitle("Advance 50%", for: .normal)
            
            // show advance price
            let divide_total_by_50 = total_amount_calculate/2
            print(divide_total_by_50 as Any)
            
            
            
            self.btnmakePayment.setTitle("Make Advanced Payment $\(divide_total_by_50) ", for: .normal)
            self.total_price = String(divide_total_by_50)
            
            
            
            cell.btnTotal.addTarget(self, action: #selector(total_payment_click), for: .touchUpInside)
            
            cell.btnAdvance.addTarget(self, action: #selector(advance_amount_pay), for: .touchUpInside)
            // self.btnmakePayment.addTarget(self, action: #selector(confirm_payment), for: .touchUpInside)
            
        }
        
        cell.txtCardNumber.addTarget(self, action: #selector(PaymentVC.textFieldDidChange(_:)), for: .editingChanged)
        cell.txtExpDate.addTarget(self, action: #selector(PaymentVC.textFieldDidChange2(_:)), for: .editingChanged)
        
        return cell
    }
    
    @objc func advance_amount_pay() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PaymentTableViewCell
        
        let x : Int = self.dict_get_table_Details["seatPrice"] as! Int
        let myString = String(x)
        
        let x_2 : Int = self.dict_get_table_Details["totalSeat"] as! Int
        let myString_2 = String(x_2)
        
        let total_amount_calculate = Double(myString)!*Double(myString_2)!
        // print(total_amount_calculate as Any)
        
        
        
        
        let divide_total_by_50 = total_amount_calculate/2
        print(divide_total_by_50 as Any)
        
         cell.btnTotal.setTitle("Pay Full : $\(total_amount_calculate)", for: .normal)
        
        self.btnmakePayment.setTitle("Make Advanced Payment of $\(divide_total_by_50) ", for: .normal)
        self.total_price = String(divide_total_by_50)
        
        cell.btnAdvance.backgroundColor = NAVIGATION_COLOR
        cell.btnAdvance.layer.cornerRadius = 8.0
        cell.btnAdvance.clipsToBounds = true
        cell.btnAdvance.tintColor = .white
        
        cell.btnTotal.backgroundColor = .lightGray
        cell.btnTotal.layer.cornerRadius = 8.0
        cell.btnTotal.clipsToBounds = true
        cell.btnTotal.tintColor = .black
        
        
        self.is_full_payment_status = "2"
    }
    
    @objc func total_payment_click() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PaymentTableViewCell
        
        cell.btnTotal.backgroundColor = NAVIGATION_COLOR
        cell.btnTotal.layer.cornerRadius = 8.0
        cell.btnTotal.clipsToBounds = true
        cell.btnTotal.tintColor = .white
        
        cell.btnAdvance.backgroundColor = .lightGray
        cell.btnAdvance.layer.cornerRadius = 8.0
        cell.btnAdvance.clipsToBounds = true
        cell.btnAdvance.tintColor = .black
        
        let x : Int = self.dict_get_table_Details["seatPrice"] as! Int
        let myString = String(x)
        
        let x_2 : Int = self.dict_get_table_Details["totalSeat"] as! Int
        let myString_2 = String(x_2)
        
        let total_amount_calculate = Double(myString)!*Double(myString_2)!
        // print(total_amount_calculate as Any)
        
        
        self.btnmakePayment.setTitle("Make Full Payment of  $\(total_amount_calculate)", for: .normal)
        self.total_price = String(total_amount_calculate)
        
        self.is_full_payment_status = "1"
    }
    
    @objc func full_amount_pay() {
        
    }
    
    @objc func confirm_payment() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BookingSuccessVC") as? BookingSuccessVC
         push!.str_booked_price = self.total_price
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PaymentTableViewCell
        cell.lblCardNumber.isHidden = false
        cell.lblCardNumber.text! = cell.txtCardNumber.text!
        
    }
    
    @objc func textFieldDidChange2(_ textField: UITextField) {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PaymentTableViewCell
        
            cell.lblCardExpDate.isHidden = false
            cell.lblCardExpDate.text! = cell.txtExpDate.text!
        
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! PaymentTableViewCell
        
        if textField == cell.txtCardNumber {
            
            let first2 = String(cell.txtCardNumber.text!.prefix(2))
            
            if first2.count == 2 {
               // print("yes")
                
                let first3 = String(cell.txtCardNumber.text!.prefix(2))
               // print(first3 as Any)
                
                if first3 == "34" { // amex
                    cell.imgCardType.image = UIImage(named: "Amex")
                    self.strCardType = "amex"
                } else if first3 == "37" { // amex
                    cell.imgCardType.image = UIImage(named: "Amex")
                    self.strCardType = "amex"
                } else if first3 == "51" { // master
                    cell.imgCardType.image = UIImage(named: "Mastercard")
                    self.strCardType = "master"
                } else if first3 == "55" { // master
                    cell.imgCardType.image = UIImage(named: "Mastercard")
                    self.strCardType = "master"
                }  else if first3 == "42" { // visa
                    cell.imgCardType.image = UIImage(named: "visa")
                    self.strCardType = "visa"
                } else if first3 == "65" { // discover
                    cell.imgCardType.image = UIImage(named: "Discover")
                    self.strCardType = "discover"
                } else {
                    cell.imgCardType.image = UIImage(named: "ccCard")
                    self.strCardType = "none"
                }
                
            } else {
                // print("no")
                cell.imgCardType.image = UIImage(named: "ccCard")
            }
            
            
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            
            if self.strCardType == "amex" {
                return count <= 15
            } else {
                return count <= 16
            }
            
            
        }
        
        if textField == cell.txtExpDate {
            if string == "" {
                return true
            }

            
            let currentText = textField.text! as NSString
            let updatedText = currentText.replacingCharacters(in: range, with: string)

            textField.text = updatedText
            let numberOfCharacters = updatedText.count
            
            if numberOfCharacters == 2 {
                textField.text?.append("/")
            }
            
            cell.lblCardExpDate.text! = cell.txtExpDate.text!
        }
        
       if textField == cell.txtCVV {
           
           guard let textFieldText = textField.text,
               let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                   return false
           }
           let substringToReplace = textFieldText[rangeOfTextToReplace]
           let count = textFieldText.count - substringToReplace.count + string.count
           return count <= 3
       }
        
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 800
    }
    
    
}

// 11682190887

// 001729

// MEENAKSHI

extension Date {

 static func getCurrentDate() -> String {

        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "yyyy-MM-dd"

        return dateFormatter.string(from: Date())
    }
}
