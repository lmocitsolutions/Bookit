//
//  WelcomeVC.swift
//  Bookit
//
//  Created by Ranjan on 18/12/21.
//

import UIKit

var selectedProfile:String = ""

class WelcomeVC: UIViewController {
    
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
                navigationBar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
                navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                navigationBar.layer.shadowOpacity = 1.0
                navigationBar.layer.shadowRadius = 15.0
                navigationBar.layer.masksToBounds = false
            }
        }
            
       /* @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }*/
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "WELCOME"
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    @IBOutlet weak var btnCustomer:UIButton!{
        didSet{
            btnCustomer.backgroundColor = NAVIGATION_COLOR
            btnCustomer.setTitle("CUSTOMER", for: .normal)
            btnCustomer.layer.cornerRadius = 27.5
            btnCustomer.clipsToBounds = true
            
            btnCustomer.addTarget(self, action: #selector(btnCustomerTapped), for: .touchUpInside)
        }
        
    }
    @IBOutlet weak var btnNightClubs:UIButton!{
        didSet{
            btnNightClubs.backgroundColor = BUTTON_DARK_APP_COLOR
            btnNightClubs.setTitle("Night Clubs", for: .normal)
            btnNightClubs.layer.cornerRadius = 27.5
            btnNightClubs.clipsToBounds = true
            btnNightClubs.addTarget(self, action: #selector(btnNightClubsTapped), for: .touchUpInside)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = APP_BASIC_COLOR
        
        self.rememberMe()
        
    }
    
    @objc func rememberMe() {
        // MARK:- PUSH -
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NPHomeVC")
            self.navigationController?.pushViewController(push, animated: false)
            
        }
    }
    
    @objc func btnCustomerTapped(){
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "GetStartedNowVC") as? GetStartedNowVC
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        selectedProfile = "customer"
    }
    
    @objc func btnNightClubsTapped(){
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ClubDetailsPhotos") as? ClubDetailsPhotos
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
        selectedProfile = "nightclub"
        
    }
    
    

}
