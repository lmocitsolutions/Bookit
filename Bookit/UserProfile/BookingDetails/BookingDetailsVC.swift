//
//  BookingDetailsVC.swift
//  Bookit
//
//  Created by Ranjan on 27/12/21.
//

import UIKit
import SDWebImage

class BookingDetailsVC: UIViewController {
    
    var dict_get_booking_details:NSDictionary!
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
                navigationBar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
                navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                navigationBar.layer.shadowOpacity = 1.0
                navigationBar.layer.shadowRadius = 15.0
                navigationBar.layer.masksToBounds = false
            }
        }
            
       @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "CLUB DETAIL"
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
   
    @IBOutlet weak var tablView:UITableView!{
        didSet {
            tablView.delegate = self
            tablView.dataSource = self
            tablView.backgroundColor =  .white
        }
    }

    
    @IBOutlet weak var btnPay:UIButton! {
        didSet {
            
            btnPay.backgroundColor = BUTTON_DARK_APP_COLOR
            btnPay.tintColor = .white
            
            // btnPay.titleLabel?.font = UIFont.systemFont(ofSize: 22.0, weight: .bold)
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        self.navigationController?.isNavigationBarHidden = true
        self.tablView.separatorColor = .clear
        
        self.btnBack.addTarget(self, action: #selector(back_click_method), for: .touchUpInside)
        // print(self.dict_get_booking_details as Any)
        
        /*
         Clubbanner = "";
         ClubcontactNumber = 7428171872;
         Clubemail = "raushan@mailinator.com";
         ClubfullName = "Raushan Kumar";
         Clubimage = "https://demo4.evirtualservices.net/bookit/img/uploads/users/16400815972.jpg";
         ClublastName = "";
         Tableimage = "https://demo4.evirtualservices.net/bookit/img/uploads/table/16400820692.jpg";
         TableseatPrice = 75;
         TabletotalSeat = 5;
         Userimage = "";
         advancePayment = 75;
         bokingDate = "";
         bookingId = 7;
         clubId = 2;
         clubTableId = 2;
         contactNumber = 1232142314;
         created = "2022-01-18 18:15:00";
         email = "ios@gmail.com";
         fullName = ios;
         fullPaymentStatus = 2;
         lastName = "";
         seatPrice = 75;
         tableName = Xyz;
         totalAmount = 75;
         totalSeat = 5;
         userId = 6;
         */
        
        
        let x : Int = self.dict_get_booking_details["fullPaymentStatus"] as! Int
        let myString = String(x)
        
        if myString == "2" {
            // advance
            
            // let x : Int = self.dict_get_booking_details["totalAmount"] as! Int
            // let myString = String(x)
            
            // let x_2 : Int = self.dict_get_booking_details["advancePayment"] as! Int
            // let myString_2 = String(x_2)
            
            // total amount
            let total_amount_get:String!
            
            if self.dict_get_booking_details["totalAmount"] is String {
                print("Yes, it's a String")

                total_amount_get = (self.dict_get_booking_details["totalAmount"] as! String)
                
            } else if self.dict_get_booking_details["totalAmount"] is Int {
                print("It is Integer")
                            
                let x2 : Int = (self.dict_get_booking_details["totalAmount"] as! Int)
                let myString2 = String(x2)
                
                total_amount_get = String(myString2)
                
            } else {
                print("i am number")
                            
                let temp:NSNumber = self.dict_get_booking_details["totalAmount"] as! NSNumber
                let tempString = temp.stringValue
                
                total_amount_get = String(tempString)
            }
            
            
            // advance payment
            let advance_payment_get:String!
            
            if self.dict_get_booking_details["advancePayment"] is String {
                print("Yes, it's a String")

                advance_payment_get = (self.dict_get_booking_details["advancePayment"] as! String)
                
            } else if self.dict_get_booking_details["advancePayment"] is Int {
                print("It is Integer")
                            
                let x2 : Int = (self.dict_get_booking_details["advancePayment"] as! Int)
                let myString2 = String(x2)
                
                advance_payment_get = String(myString2)
                
            } else {
                print("i am number")
                            
                let temp:NSNumber = self.dict_get_booking_details["advancePayment"] as! NSNumber
                let tempString = temp.stringValue
                
                advance_payment_get = String(tempString)
            }
            
            let get_pending_amount = Double(total_amount_get)!-Double(advance_payment_get)!
            
            self.btnPay.setTitle("Pending Amount : $\(get_pending_amount)", for: .normal)
            self.btnPay.isUserInteractionEnabled = true
            
        } else {
            // full payment
            
            self.btnPay.setTitle("Payment Done", for: .normal)
            self.btnPay.backgroundColor = .systemGreen
            self.btnPay.isUserInteractionEnabled = false
        }
        
    }
    
    @objc func back_click_method() {
        self.navigationController?.popViewController(animated: true)
    }
  

}

//MARK:- TABLE VIEW -
extension BookingDetailsVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:BookingDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BookingDetailsTableCell") as! BookingDetailsTableViewCell
        
        cell.backgroundColor = .white
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        // cell.imgBG.image = UIImage(named: "bar")
        
       // cell.btnSignUp.addTarget(self, action: #selector(btnSignUpTapped), for: .touchUpInside)
        
        
        cell.lblName.text = (self.dict_get_booking_details["ClubfullName"] as! String)
        cell.btnPhone.setTitle((self.dict_get_booking_details["ClubcontactNumber"] as! String), for: .normal)
        cell.btnLocation.setTitle("N.A.", for: .normal)
        
        let x_3 : Int = self.dict_get_booking_details["totalAmount"] as! Int
        let myString_3 = String(x_3)
        
        let x_4 : Int = self.dict_get_booking_details["TabletotalSeat"] as! Int
        let myString_4 = String(x_4)
        
        // let totalAmount = myString_2
        
        // let advance:Double = myString_3
        
        // let x : Int = self.dict_get_booking_details["userId"] as! Int
        // let myString = String(x)
        
        cell.lblTableNum.text   = (self.dict_get_booking_details["tableName"] as! String)
        
        if (self.dict_get_booking_details["bokingDate"] as! String) == "" {
            cell.lblDate.text       = "N.A."
        } else {
            cell.lblDate.text       = (self.dict_get_booking_details["bokingDate"] as! String)
        }
        
        
        cell.lblTime.text       = "N.A."
        cell.lblTotalSeat.text  = myString_4
        cell.lblTotalAmount.text = "$"+myString_3
        
        // let x_2 : Int = self.dict_get_booking_details["advancePayment"] as! Int
        // let myString_2 = String(x_2)
        
        if self.dict_get_booking_details["advancePayment"] is String {
            print("Yes, it's a String")

            cell.lblAdvancedPay.text = "$"+(self.dict_get_booking_details["advancePayment"] as! String)
            
        } else if self.dict_get_booking_details["advancePayment"] is Int {
            print("It is Integer")
                        
            let x2 : Int = (self.dict_get_booking_details["advancePayment"] as! Int)
            let myString2 = String(x2)
            // self.lblPayableAmount.text = "Membership Amount : $ "+myString2
                        
             
            cell.lblAdvancedPay.text = "$"+String(myString2)
            
        } else {
            print("i am number")
                        
            let temp:NSNumber = self.dict_get_booking_details["advancePayment"] as! NSNumber
            let tempString = temp.stringValue
            
            cell.lblAdvancedPay.text = "$"+String(tempString)
        }
        
        
        cell.imgBG.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        cell.imgBG.sd_setImage(with: URL(string: (self.dict_get_booking_details["Clubimage"] as! String)), placeholderImage: UIImage(named: "bar"))
        
        self.btnPay.addTarget(self, action: #selector(pending_amount_payment_click_method), for: .touchUpInside)
        // let remainingAmount:Double = totalAmount - advance
        
        // btnPay.setTitle("PAY REST AMOUNT- $"+String(remainingAmount), for: .normal)
        
        return cell
    }

    @objc func pending_amount_payment_click_method() {
        
        let club_name = (self.dict_get_booking_details["fullName"] as! String)
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PaymentVC") as? PaymentVC
        push!.dict_get_table_Details = self.dict_get_booking_details
        push!.get_club_name = club_name
        
        push!.am_from_which_profile = "booking_Details"
        
        self.navigationController?.pushViewController(push!, animated: true)
        
    }
    
    @objc func btnSignUpTapped() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTableVC") as? AddTableVC
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 686
    }
    
    
}

extension BookingDetailsVC: UITableViewDelegate {
    
}
