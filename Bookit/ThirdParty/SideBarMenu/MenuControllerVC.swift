//
//  MenuControllerVC.swift
//  SidebarMenu
//
//  Created by Apple  on 16/10/19.
//  Copyright © 2019 AppCoda. All rights reserved.
//

import UIKit
//import Alamofire
//import SDWebImage

class MenuControllerVC: UIViewController {
    
    

    let cellReuseIdentifier = "menuControllerVCTableCell"
    
    var bgImage: UIImageView?
    
    var roleIs:String!
    
    @IBOutlet weak var navigationBar:UIView!{
        didSet{
            navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
    
    @IBOutlet weak var viewUnderNavigation:UIView! {
        didSet {
            
            viewUnderNavigation.backgroundColor = BUTTON_DARK_APP_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "DASHBOARD"
            lblNavigationTitle.textColor = .white
        }
    }
    
    @IBOutlet weak var imgSidebarMenuImage:UIImageView! {
        didSet {
           
            //imgSidebarMenuImage.layer.borderWidth = 5.0
            //imgSidebarMenuImage.layer.borderColor = UIColor.white.cgColor
            imgSidebarMenuImage.image = UIImage(named: "logo")
        }
    }
    
    
    // Customer
    var arrCustomerTitle = ["Dashboard", "Edit Profile", "Booking History","Help",/* "E Shop", "Order History", */"Change Password","Logout"]
    
    // CustomerImage
    var arrCustomerImage = ["Dashboard", "Edit Profile","Manage Inventory", "Booking History","Help",/* "E Shop", "Order History", */"Change Password","Logout"]
    
    //Club
    
    var arrClubTitle = ["Dashboard", "Edit Profile", "Booking History","Manage Table/Price","Earnings","Cashout",/* "E Shop", "Order History", */"Change Password","Logout","Help"]
    
   @IBOutlet weak var lblUserName:UILabel! {
       
        didSet {
            
            lblUserName.text = "Dance Club"
            lblUserName.textColor = .white
            
        }
    }
    
    
    @IBOutlet weak var imgClub:UIImageView!{
        
        didSet{
            
            imgClub.clipsToBounds = true
            imgClub.layer.cornerRadius = 10.0
            imgClub.layer.borderWidth = 5
            imgClub.layer.borderColor = UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1).cgColor
        }
        
    }
   
    
    @IBOutlet weak var lblPhoneNumber:UILabel! {
        didSet {
            lblPhoneNumber.textColor = .white
            //lblPhoneNumber.text = "1800-4267-3923"
            
            lblPhoneNumber.isHidden = true
        }
    }
    
    @IBOutlet var btnLocation:UIButton!{
        didSet{
            btnLocation.tintColor = .white
        }
        
    }
    
    @IBOutlet var menuButton:UIButton!
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
            tbleView.tableFooterView = UIView.init()
            tbleView.backgroundColor = BUTTON_DARK_APP_COLOR
            // tbleView.separatorInset = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
            tbleView.separatorColor = .systemGray6
        }
    }
    
    @IBOutlet weak var lblMainTitle:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideBarMenuClick()
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        self.tbleView.separatorColor = .white // UIColor.init(red: 60.0/255.0, green: 110.0/255.0, blue: 160.0/255.0, alpha: 1)
        
        //self.tbleView.backgroundColor = UIColor.init(red: 71.0/256.0, green: 119.0/256.0, blue: 81.0/256.0, alpha: 1)
        
        self.view.backgroundColor = BUTTON_DARK_APP_COLOR // NAVIGATION_BACKGROUND_COLOR
        
        self.sideBarMenuClick()
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        // print(person as Any)
            
            if (person["role"] as! String) == "Customer" {
                
                imgClub.isHidden = true
                lblUserName.isHidden = true
                btnLocation.isHidden = true
            }
            
            else{
                
                imgSidebarMenuImage.isHidden = true
                imgClub.image = UIImage(named: "bar")
                lblUserName.text = "Dance Again Club"
                btnLocation.setTitle("Brooklyn, New York, USA", for: .normal)
               // btnLocation.isEnabled = false
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            if person["role"] as! String == "Customer" {
               
               self.lblUserName.text = (person["fullName"] as! String)
               // self.lblPhoneNumber.text = (person["contactNumber"] as! String)
               
               //self.imgSidebarMenuImage.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
               //self.imgSidebarMenuImage.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "logo"))
               
            }
             
             else {
                 // self.lblUserName.text = "Ranjan"
                 // self.lblPhoneNumber.text = "1321321"
                 
               /* self.lblUserName.text = (person["fullName"] as! String)
                self.lblPhoneNumber.text = (person["contactNumber"] as! String)
                
                self.imgSidebarMenuImage.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                self.imgSidebarMenuImage.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "logo"))*/
            }
             
        }
        
    }
    
    
   
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @objc func sideBarMenuClick() {
        
        if revealViewController() != nil {
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
}

extension MenuControllerVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
            
            if (person["role"] as! String) == "Customer" {
                
                return arrCustomerTitle.count
            }
            else {
                return arrClubTitle.count
            }
            
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MenuControllerVCTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MenuControllerVCTableCell
        
        cell.backgroundColor = BUTTON_DARK_APP_COLOR
      
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        // print(person as Any)
            
            if (person["role"] as! String) == "Customer"{
                
        cell.lblName.text = arrCustomerTitle[indexPath.row]
            
        }
        
        else {
            cell.lblName.text = arrClubTitle[indexPath.row]
        }
        
        }
        return cell
            
            
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
            
            if (person["role"] as! String) == "Customer" {
                
                if arrCustomerTitle[indexPath.row] == "Booking History" {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                    self.view.window?.rootViewController = sw
                    let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "BookingHistoryVC")
                    let navigationController = UINavigationController(rootViewController: destinationController!)
                    sw.setFront(navigationController, animated: true)
                    
                } else if arrCustomerTitle[indexPath.row] == "Dashboard" {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                    self.view.window?.rootViewController = sw
                    let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "NPHomeVC")
                    let navigationController = UINavigationController(rootViewController: destinationController!)
                    sw.setFront(navigationController, animated: true)
                    
                }
                
            } else {
                
            }
            
        }
        
        /*if arrCustomerTitle[indexPath.row] == "Change Password" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
            
        }
        
        else if arrCustomerTitle[indexPath.row] == "Booking History"
            
        {
           
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
           self.view.window?.rootViewController = sw
           let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "BookingHistoryVC")
           let navigationController = UINavigationController(rootViewController: destinationController!)
           sw.setFront(navigationController, animated: true)
           
       }
        else if arrCustomerTitle[indexPath.row] == "Help"
            
        {
           
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
           self.view.window?.rootViewController = sw
           let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "HelpVC")
           let navigationController = UINavigationController(rootViewController: destinationController!)
           sw.setFront(navigationController, animated: true)
           
       }
        
        else if arrCustomerTitle[indexPath.row] == "Dashboard"
            
        {
           
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
           self.view.window?.rootViewController = sw
           let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "NPHomeVC")
           let navigationController = UINavigationController(rootViewController: destinationController!)
           sw.setFront(navigationController, animated: true)
           
       }*/
            
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension MenuControllerVC: UITableViewDelegate {
    
}
