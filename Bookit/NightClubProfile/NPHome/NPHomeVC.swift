//
//  NPHomeVC.swift
//  Bookit
//
//  Created by Ranjan on 22/12/21.
//

import UIKit
import Alamofire
import SDWebImage
import SPConfetti
import MapKit

class NPHomeVC: UIViewController {
    
    var arr_mut_dashboard_Data:NSMutableArray! = []
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
                navigationBar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
                navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                navigationBar.layer.shadowOpacity = 1.0
                navigationBar.layer.shadowRadius = 15.0
                navigationBar.layer.masksToBounds = false
            }
        }
            
       @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
                
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "HOME"
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    @IBOutlet weak var viwTop:UIView!{
        didSet{
            viwTop.backgroundColor = NAVIGATION_COLOR
        }
    }
    
    @IBOutlet weak var searchBar:UISearchBar!{
        didSet{
            
            searchBar.layer.cornerRadius = 8.0
            searchBar.clipsToBounds = true
        }
    }
   
    @IBOutlet weak var tablView:UITableView!{
        didSet {
            // tablView.delegate = self
            // tablView.dataSource = self
            tablView.backgroundColor =  .clear//APP_BASIC_COLOR
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        // Available `.arc`, `.star`, `.heart`, `.circle`, `.triangle` and `.polygon`.
        // SPConfetti.startAnimating(.centerWidthToUp, particles: [.heart])
        
        /*let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: cordinate, addressDictionary: nil))
                    mapItem.name = "test"
                    mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])*/
        
        
        // UIApplication.shared.open(URL(string: "http://maps.google.com/maps?q=loc:\("28.99"),\("77.14")&zoom=14&views=traffic&q=\("30.44"),\("21.00")")!, options: [:], completionHandler: nil)
        
        // self.openMapButtonAction()
        
        self.view.backgroundColor = cell_bg_color//APP_BASIC_COLOR
        self.navigationController?.isNavigationBarHidden = true
        self.tablView.separatorColor = .clear
        self.sideBarMenuClick()
        
         self.customer_dashboard_wb()
    }
    
    
    @objc func sideBarMenuClick() {
        
        let defaults = UserDefaults.standard
        defaults.setValue("", forKey: "keyBackOrSlide")
        defaults.setValue(nil, forKey: "keyBackOrSlide")
        
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    @objc func customer_dashboard_wb() {
        
        self.view.endEditing(true)
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        let params = customer_dashboard(action: "clublist")
        
        print(params as Any)
        
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
            // debugPrint(response.result)
            
            switch response.result {
            case let .success(value):
                
                let JSON = value as! NSDictionary
                print(JSON as Any)
                
                var strSuccess : String!
                strSuccess = (JSON["status"]as Any as? String)?.lowercased()
                print(strSuccess as Any)
                if strSuccess == String("success") {
                    print("yes")
                    
                    ERProgressHud.sharedInstance.hide()
                    
                    var ar : NSArray!
                    ar = (JSON["data"] as! Array<Any>) as NSArray
                    self.arr_mut_dashboard_Data.addObjects(from: ar as! [Any])
                    
                    self.tablView.delegate = self
                    self.tablView.dataSource = self
                    self.tablView.reloadData()
                    
                    /*var dict: Dictionary<AnyHashable, Any>
                    dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                    
                    let defaults = UserDefaults.standard
                    defaults.setValue(dict, forKey: "keyLoginFullData")
                    
                    
                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NPHomeVC")
                    self.navigationController?.pushViewController(push, animated: true)
                    */
                    
                } else {
                    print("no")
                    //  ERProgressHud.sharedInstance.hide()
                    
                    var strSuccess2 : String!
                    strSuccess2 = JSON["msg"]as Any as? String
                    
                    if strSuccess2 == "Your Account is Inactive. Please contact admin.!!" ||
                        strSuccess2 == "Your Account is Inactive. Please contact admin.!" ||
                        strSuccess2 == "Your Account is Inactive. Please contact admin." {
                        
                        
                    } else {
                        
                        let alert = UIAlertController(title: String(strSuccess).uppercased(), message: String(strSuccess2), preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        
                        self.present(alert, animated: true)
                        
                    }
                }
                
            case let .failure(error):
                print(error)
                ERProgressHud.sharedInstance.hide()
                
                // Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
            }
        }
        // }
    }

    
    
    func openMapButtonAction() {
            let latitude = 45.5088
            let longitude = -73.554

            /*let appleURL = "http://maps.apple.com/?daddr=\(latitude),\(longitude)"
            let googleURL = "comgooglemaps://?daddr=\(latitude),\(longitude)&directionsmode=driving"
            let wazeURL = "waze://?ll=\(latitude),\(longitude)&navigate=false"

            let googleItem = ("Google Map", URL(string:googleURL)!)
            let wazeItem = ("Waze", URL(string:wazeURL)!)
            var installedNavigationApps = [("Apple Maps", URL(string:appleURL)!)]

            if UIApplication.shared.canOpenURL(googleItem.1) {
                installedNavigationApps.append(googleItem)
            }

            if UIApplication.shared.canOpenURL(wazeItem.1) {
                installedNavigationApps.append(wazeItem)
            }

            let alert = UIAlertController(title: "Selection", message: "Select Navigation App", preferredStyle: .actionSheet)
            for app in installedNavigationApps {
                let button = UIAlertAction(title: app.0, style: .default, handler: { _ in
                    UIApplication.shared.open(app.1, options: [:], completionHandler: nil)
                })
                alert.addAction(button)
            }
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(cancel)
            present(alert, animated: true)*/
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                UIApplication.shared.openURL(NSURL(string:
                                                    "comgooglemaps://?saddr=&daddr=\(Float(28.11)),\(Float(77.1))&directionsmode=driving")! as URL)

            } else {
                // if GoogleMap App is not installed
                UIApplication.shared.openURL(NSURL(string:
                                                    "https://maps.google.com/?q=@\(Float(30.99)),\(Float(78))")! as URL)
            }
        
        
        
        
        }
    
    
}


//MARK:- TABLE VIEW -
extension NPHomeVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_mut_dashboard_Data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:NPHomeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NPHomeTableCell") as! NPHomeTableViewCell
        
        cell.backgroundColor = .clear //APP_BASIC_COLOR
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        let item = self.arr_mut_dashboard_Data[indexPath.row] as? [String:Any]
        print(item as Any)
        
        /*
         ["Userimage": https://demo4.evirtualservices.net/bookit/img/uploads/users/16400815972.jpg, "openTime": 10:30 AM, "state": Delhi, "fullName": Raushan Kumar, "closeTime": 11:30 PM, "contactNumber": 7428171872, "userId": 2, "banner": , "email": raushan@mailinator.com]
         */
        
        cell.btnDistance.setTitle("2 miles", for: .normal)
        
        cell.lblName.text = (item!["fullName"] as! String)
        cell.btnPhone.setTitle((item!["contactNumber"] as! String), for: .normal)
        
        cell.imgBG.image = UIImage(named: "bar")
        
        cell.imgBG.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        cell.imgBG.sd_setImage(with: URL(string: (item!["Userimage"] as! String)), placeholderImage: UIImage(named: "bar"))
        
       // cell.btnSignUp.addTarget(self, action: #selector(btnSignUpTapped), for: .touchUpInside)
        
        return cell
    }

    @objc func btnSignUpTapped() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTableVC") as? AddTableVC
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = self.arr_mut_dashboard_Data[indexPath.row] as? [String:Any]
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NPClubDetailVC") as? NPClubDetailVC
        push!.dict_get_club_details = item! as NSDictionary
        self.navigationController?.pushViewController(push!, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260
    }
    
    
}

extension NPHomeVC: UITableViewDelegate {
    
}
