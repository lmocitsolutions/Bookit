//
//  NPClubDetailVC.swift
//  Bookit
//
//  Created by Ranjan on 22/12/21.
//

import UIKit
import SDWebImage

class NPClubDetailVC: UIViewController {
    
    var dict_get_club_details:NSDictionary!
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
                navigationBar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
                navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                navigationBar.layer.shadowOpacity = 1.0
                navigationBar.layer.shadowRadius = 15.0
                navigationBar.layer.masksToBounds = false
            }
        }
            
       @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "CLUB DETAIL"
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
   
    @IBOutlet weak var tablView:UITableView! {
        didSet {
            tablView.delegate = self
            tablView.dataSource = self
            tablView.backgroundColor = .white
        }
    }

     

    override func viewDidLoad() {
        super.viewDidLoad()

        //  print(self.dict_get_club_details as Any)
        
        self.view.backgroundColor = APP_BASIC_COLOR
        self.navigationController?.isNavigationBarHidden = true
        self.tablView.separatorColor = .clear
        btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
    }
    
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }

}


//MARK:- TABLE VIEW -
extension NPClubDetailVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:NPClubDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NPClubDetailTableCell") as! NPClubDetailTableViewCell
        
        cell.backgroundColor = APP_BASIC_COLOR
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.imgBG.image = UIImage(named: "bar")
        
        cell.btnDistance.setTitle("2 miles", for: .normal)
        
        cell.lblName.text = (self.dict_get_club_details!["fullName"] as! String)
        cell.btnPhone.setTitle((self.dict_get_club_details!["contactNumber"] as! String), for: .normal)
        
        cell.imgBG.image = UIImage(named: "bar")
        
        cell.imgBG.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        cell.imgBG.sd_setImage(with: URL(string: (self.dict_get_club_details!["Userimage"] as! String)), placeholderImage: UIImage(named: "bar"))
        
        cell.btnPhotos.addTarget(self, action: #selector(btnPhotosTapped), for: .touchUpInside)
        cell.btnTables.addTarget(self, action: #selector(btnTablesTapped), for: .touchUpInside)
        cell.btnReviews.addTarget(self, action: #selector(btnReviewsTapped), for: .touchUpInside)
        cell.btnDirections.addTarget(self, action: #selector(btnDirectionsTapped), for: .touchUpInside)
        
        return cell
    }

    @objc func btnPhotosTapped() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NPClubPhotosVC") as? NPClubPhotosVC
        
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func btnTablesTapped() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NPClubTableDetailVC") as? NPClubTableDetailVC
        push!.club_Details = self.dict_get_club_details
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func btnReviewsTapped() {
        
        //
        
        let x : Int = dict_get_club_details["userId"] as! Int
        let myString = String(x)
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NPClubReviewVC") as? NPClubReviewVC
        push!.get_club_id = myString
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func btnDirectionsTapped() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NPClubDirectionVC") as? NPClubDirectionVC
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 800
    }
    
    
}

extension NPClubDetailVC: UITableViewDelegate {
    
}
