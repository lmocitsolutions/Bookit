//
//  NPClubDetailTableViewCell.swift
//  Bookit
//
//  Created by Ranjan on 22/12/21.
//

import UIKit

class NPClubDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viw:UIView!{
        didSet{
            viw.backgroundColor = .clear
            
            viw.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
            viw.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            viw.layer.shadowOpacity = 1.0
            viw.layer.shadowRadius = 15.0
            viw.layer.masksToBounds = false
        }
    }
    
    @IBOutlet weak var imgBG:UIImageView! {
        didSet{
           // imgBG.layer.cornerRadius = 8.0
            //imgBG.clipsToBounds = true
            //imgBG.layer.borderWidth = 0.8
            //imgBG.layer.borderColor = UIColor.systemGray6.cgColor
           // imageView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnShare:UIButton!{
        didSet{
            //btnShare.layer.cornerRadius = 4.0
            //btnShare.clipsToBounds = true
            
            btnShare.backgroundColor = .white
            btnShare.tintColor = .systemYellow
            btnShare.layer.cornerRadius = 8
            btnShare.clipsToBounds = true
            
        }
    }
    
    @IBOutlet weak var btnLike:UIButton!{
        didSet{
            //btnShare.layer.cornerRadius = 4.0
            //btnShare.clipsToBounds = true
            
            btnLike.backgroundColor = .white
            btnLike.tintColor = .darkGray
            btnLike.layer.cornerRadius = 8
            btnLike.clipsToBounds = true
            
        }
    }
    
    @IBOutlet weak var btnStarOne:UIButton!{
        didSet{
            //btnShare.layer.cornerRadius = 4.0
            //btnShare.clipsToBounds = true
        }
    }
    @IBOutlet weak var btnStarTwo:UIButton!{
        didSet{
            //btnShare.layer.cornerRadius = 4.0
            //btnShare.clipsToBounds = true
        }
    }
    @IBOutlet weak var btnStarThree:UIButton!{
        didSet{
            //btnShare.layer.cornerRadius = 4.0
            //btnShare.clipsToBounds = true
        }
    }
    @IBOutlet weak var btnStarFour:UIButton!{
        didSet{
            //btnShare.layer.cornerRadius = 4.0
            //btnShare.clipsToBounds = true
        }
    }
    @IBOutlet weak var btnStarFive:UIButton!{
        didSet{
            //btnShare.layer.cornerRadius = 4.0
            //btnShare.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblRating:UILabel!{
        didSet{
            lblRating.textColor = .white
        }
    }
    
    @IBOutlet weak var lblName:UILabel!{
        didSet{
            lblName.textColor = .white
        }
    }
    
    @IBOutlet weak var btnPhone:UIButton!{
        didSet{
            //btnShare.layer.cornerRadius = 4.0
            //btnShare.clipsToBounds = true
            //btnPhone.setTitle("+", for: <#T##UIControl.State#>)
            btnPhone.backgroundColor = .clear
            btnPhone.tintColor = .white
        }
    }
    
    @IBOutlet weak var btnLocation:UIButton!{
        didSet{
            //btnShare.layer.cornerRadius = 4.0
            //btnShare.clipsToBounds = true
            //btnPhone.setTitle("+", for: <#T##UIControl.State#>)
            btnLocation.backgroundColor = .clear
            btnLocation.tintColor = .white
        }
    }
    
    @IBOutlet weak var btnDistance:UIButton!{
        didSet{
            //btnShare.layer.cornerRadius = 4.0
            //btnShare.clipsToBounds = true
            //btnPhone.setTitle("+", for: <#T##UIControl.State#>)
            btnDistance.backgroundColor = .systemGreen
            btnDistance.tintColor = .white
        }
    }
    
    @IBOutlet weak var btnTables:UIButton! {
        didSet {
            btnTables.layer.cornerRadius = 0
            btnTables.clipsToBounds = true
            btnTables.setTitle("TABLES", for: .normal)
            btnTables.backgroundColor =  BUTTON_DARK_APP_COLOR
            btnTables.tintColor = .white
        }
    }
    
    @IBOutlet weak var btnReviews:UIButton!{
        didSet {
            btnReviews.layer.cornerRadius = 0
            btnReviews.clipsToBounds = true
            btnReviews.setTitle("REVIEWS", for: .normal)
            btnReviews.backgroundColor =  BUTTON_DARK_APP_COLOR
            btnReviews.tintColor = .white
        }
    }
    
    @IBOutlet weak var btnDirections:UIButton!{
        didSet {
            btnDirections.layer.cornerRadius = 0
            btnDirections.clipsToBounds = true
            btnDirections.setTitle("DIRECTIONS", for: .normal)
            btnDirections.backgroundColor =  BUTTON_DARK_APP_COLOR
            btnDirections.tintColor = .white
        }
    }
    
    @IBOutlet weak var btnPhotos:UIButton!{
        didSet {
            btnPhotos.layer.cornerRadius = 0
            btnPhotos.clipsToBounds = true
            btnPhotos.setTitle("PHOTOS", for: .normal)
            btnPhotos.backgroundColor =  BUTTON_DARK_APP_COLOR
            btnPhotos.tintColor = .white
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
