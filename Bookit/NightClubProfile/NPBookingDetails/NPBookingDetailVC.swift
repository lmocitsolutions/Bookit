//
//  NPBookingDetailVC.swift
//  Bookit
//
//  Created by Ranjan on 28/12/21.
//

import UIKit

class NPBookingDetailVC: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
                navigationBar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
                navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                navigationBar.layer.shadowOpacity = 1.0
                navigationBar.layer.shadowRadius = 15.0
                navigationBar.layer.masksToBounds = false
            }
        }
            
       @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "CLUB DETAIL"
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
   
    @IBOutlet weak var tablView:UITableView!{
        didSet {
            tablView.delegate = self
            tablView.dataSource = self
            tablView.backgroundColor =  .white
        }
    }
    
    @IBOutlet weak var btnReport:UIButton!{
        didSet{
            
            btnReport.backgroundColor = .systemYellow
            btnReport.tintColor = .black
            btnReport.layer.cornerRadius = 15.0
            btnReport.clipsToBounds = true
        }
    }

    
    @IBOutlet weak var btnPay:UIButton!{
        didSet{
            
            btnPay.backgroundColor = BUTTON_DARK_APP_COLOR
            btnPay.tintColor = .white
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        super.viewDidLoad()

        self.view.backgroundColor = .white
        self.navigationController?.isNavigationBarHidden = true
        self.tablView.separatorColor = .clear
    }
    


}


//MARK:- TABLE VIEW -
extension NPBookingDetailVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:NPBookingDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NPBookingDetailTableCell") as! NPBookingDetailTableViewCell
        
        cell.backgroundColor = .white
      
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.imgProfile.image = UIImage(named: "dan")
        
       // cell.btnSignUp.addTarget(self, action: #selector(btnSignUpTapped), for: .touchUpInside)
        
        let totalAmount:Double = 26.00
        
        let advance:Double = 10.00
        
        let commison:Double = 12.99
        
        cell.lblTableNum.text = "1"
        cell.lblDate.text = "Dec 26 2021"
        cell.lblTime.text = "09:30 PM"
        cell.lblTotalSeat.text = "3"
        cell.lblTotalAmount.text = "$" + String(totalAmount)
        cell.lblAdvancedPay.text = "$" + String(advance)
        
        cell.lblBookitFee.text = "$" + String(commison)
        
        let remainingAmount:Double = (totalAmount+commison) - (advance)
        
        btnPay.setTitle("PENDING AMOUNT- $" + "209.99", for: .normal)
        cell.btnLocation.setTitle("Secto 11 Noida 201301 UP", for: .normal)
        
        return cell
    }

    @objc func btnSignUpTapped(){
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTableVC") as? AddTableVC
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 660
    }
    
    
}

extension NPBookingDetailVC: UITableViewDelegate {
    
}
